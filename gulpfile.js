let preprocessor = 'scss', // Preprocessor (sass, less, styl); 'sass' also work with the Scss syntax in blocks/ folder.
	fileswatch = 'html,htm,txt,json,md,woff2' // List of files extensions for watching & hard reload

const {src, dest, parallel, series, watch} = require('gulp')
const browserSync = require('browser-sync').create()
const bssi = require('browsersync-ssi')
const ssi = require('ssi')
const webpack = require('webpack-stream')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const newer = require('gulp-newer')
const rsync = require('gulp-rsync')
const del = require('del')
const postcss = require("gulp-postcss");
const purgecss = require('gulp-purgecss');
const cleancss = require('gulp-clean-css');
const hash_src = require("gulp-hash-src");

function browsersync() {
	browserSync.init({
		server: {
			baseDir: 'src/',
			middleware: bssi({baseDir: 'src/', ext: '.html'})
		},
		// tunnel: 'yousutename', // Attempt to use the URL https://yousutename.loca.lt
		notify: false,
		online: false
	})
}

function scripts() {
	return src(['src/js/*.js', '!src/js/*.min.js'])
		.pipe(webpack({
			mode: 'production',
			module: {
				rules: [
					{
						test: /\.(js)$/,
						exclude: /(node_modules)/,
						loader: 'babel-loader',
						query: {
							presets: ['@babel/env'],
							plugins: ['babel-plugin-root-import']
						}
					}
				]
			},
			output: {
				filename: 'src.min.js'
			}
		})).on('error', function handleError() {
			this.emit('end')
		})
		.pipe(dest('src/js'))
		.pipe(browserSync.stream())
}

function prepareStyles() {
	return src('src/styles/main.scss')
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(postcss())
		.pipe(rename('main.css'))
		.pipe(dest('src/css'))
	;
}

function styles() {
	return prepareStyles()
		.pipe(browserSync.stream())
}

function buildStyles() {
	return prepareStyles()
		.pipe(purgecss({
			content: ['src/**/*.{html,js}'],
			defaultExtractor: content => {
				const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []
				const innerMatches = content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || []
				return broadMatches.concat(innerMatches)
			}
		}))
		.pipe(cleancss())
		.pipe(dest('dist/css'))
	;
}

function images() {
	return src(['src/images/src/**/*'])
		.pipe(newer('src/images/dist'))
		.pipe(imagemin())
		.pipe(dest('src/images/dist'))
		.pipe(browserSync.stream())
}

function buildCopy() {
	return src([
		'{src/js,src/css}/*.min.*',
		'src/images/**/*.*',
		'!src/images/src/**/*',
		'src/fonts/**/*'
	], {base: 'src/'})
		.pipe(dest('dist'))
}

async function buildHtml() {
	let includes = new ssi('src/', 'dist/', '/**/*.html');
	includes.compile();
	del('dist/parts', {force: true});

	return src("./dist/*.html")
		.pipe(hash_src({build_dir: "./dist", src_path: "./dist"}))
		.pipe(dest("./dist"))
	;
}

function cleanDist() {
	return del('dist/**/*', {force: true})
}

/*
|--------------------------------------------------------------------------
| Deploy
|--------------------------------------------------------------------------
|
| Собранные проект можно автоматически загрузить на сервер
| run: gulp deploy
|
| Пароль на подключение, будет запрошен после установки соединения в консоли
| Чтобы его каждый раз не вводить, на сервер необходимо добавить свой ssh ключ
|
| https://github.com/jerrysu/gulp-rsync
|
*/
function deploy() {
	return src('dist/')
		.pipe(rsync({
			root: 'dist/',
			hostname: 'username@yousite.com',
			username: 'username',
			destination: '/yousite/public_html/',
			include: [/* '*.htaccess' */], // Included files to deploy,
			exclude: ['**/Thumbs.db', '**/*.DS_Store'],
			recursive: true,
			archive: true,
			silent: false,
			compress: true
		}))
}

function startWatch() {
	watch(`src/styles/*`, {usePolling: true}, styles)
	watch(['src/js/**/*.js', '!src/js/**/*.min.js'], {usePolling: true}, scripts)
	watch('src/images/src/**/*.{jpg,jpeg,png,webp,svg,gif}', {usePolling: true}, images)
	watch(`src/**/*.{${fileswatch}}`, {usePolling: true}).on('change', browserSync.reload)
}

exports.scripts = scripts
exports.styles = styles
exports.images = images
exports.deploy = deploy
exports.assets = series(scripts, styles, images)
exports.build = series(cleanDist, parallel(scripts, buildStyles, images), buildCopy, buildHtml)
exports.default = series(scripts, styles, images, parallel(browsersync, startWatch))
