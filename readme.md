# Html Starter
Легкий, production-ready стартер Gulp.

Среда запуска с Gulp, препроцессорам Scss, clean-css, Browsersync, Autoprefixer, webpack-stream, Babel, Rsync, CSS Reboot (перезагрузка Bootstrap), импортом HTML на стороне сервера (SSI). Он использует лучшие практики для адаптивных изображений, оптимизации JavaScript и CSS.

### Как использовать
Клонировать в текущую папку и удалить все лишнее (одна команда):
```
git clone https://bitbucket.org/atholding/htmlstarter .; rm -rf trunk .gitignore readme.md .git
```

1. Перейти в корень проекта и клонировать из репозитория или скачать и распаковать
2. Установить зависимости: ```npm i```
3. Запустить: ```npm run dev```


### Основные Gulp команды
- __npm run dev__: запустить задачу gulp по умолчанию (scripts, images, styles, browsersync, startwatch). Можно работать
- __gulp scripts__, styles, images, assets: собрать ресурсы (css, js, images or all)
- __gulp deploy__: загрузить собранные проект по ssh c RSYNC (конфигурация в ./gulpfile.js)
- __npm run build__: собрать проект

### Как это работает
После установки и запуска в терминале ```npm run dev``` будет запущен локальный сервер, открыта вкладка браузера с проектом. 
Сервер будет прослушивать все изменения в исходных файлах, и автоматически обновлять страницу проекта в браузере (частями, если изменилась только часть)

После завершения разработки, проект можно собрать, выполнив в терминале ```npm run build```.
При этом, все исходные файлы будут обработаны, сжаты и помещены в ./dist директорию.

- html файлы создаются в корне папки ```./src```
- js логику размещать в ```./src/js/src.js```
- scss стили в ./src/styles/main.scss
- все исходные изображения в ./src/images/src
- все сжатые в 2 раза изображения, будут помещены при сборке автоматически в ./src/images/dist
- повторяющиеся части шаблонов, можно выносить в части, а заем подключать их
- в шаблонах можно использовать общие переменные, и затем вызывать их

```html
<!--#set var="title" value="My Site" -->
<title><!--#echo var="title" --></title>


<!--# include file="path" -->
<!--# include virtual="path" -->

<!--# set var="k" value="v" -->

<!--# echo var="n" default="default" -->

<!--# if expr="test" -->
<!--# elif expr="" -->
<!--# else -->
<!--# endif -->
```

- вся конфигурация работы, сборки и загрузки размещена в файле ./gulpfile.js
